import glob
import re
import cv2
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import h5py
import random
import sys

dataFilePath = './processed'
countPerFile = 20000
channel = sys.argv[1]

jet = plt.get_cmap("jet")
jetrange = np.linspace(0.,1., 256)
norm = matplotlib.colors.Normalize(0.,1.)
mapvals = jet(norm(jetrange))[:,:3]

# thanks to https://stackoverflow.com/questions/45177154/how-to-decode-color-mapping-in-matplotlibs-colormap
def get_value_from_cm(color):
    distance = np.sum((mapvals - color)**2, axis=1)
    return jetrange[np.argmin(distance)]
	
def inaccuracy():
	n = 10000
	f = 0
	for i in range(n):
		r = random.random()
		if(r-get_value_from_cm(jet(r)[:3]) > 0.01):
			f = f+1
	return f/n

print("Error in % while inverse mapping (> 0.01)", inaccuracy())

# Datenbereich: 931x734, Offset von links oben: 156,68
width = 931
height = 734
ySteps = height/64
xSteps = width/64

allFiles = glob.glob(f"./data/figures/{channel}/*.jpg")
allFiles = sorted(allFiles, key=lambda name: int(re.findall(r'\d+', name)[-1]))

zeroArray = np.zeros((countPerFile, 64, 64))
dataArray = zeroArray

for i, path in enumerate(allFiles):
	image = cv2.imread(path)[68:68+height,156:156+width]

	data = np.zeros([64,64],dtype=np.float32)
	for y in range(64):
		for x in range(64):
			image[int(round(y*ySteps)), int(round(x*xSteps))] = [0,255,0]
			pixelTop = int(round(y*ySteps))
			pixelLeft = int(round(x*xSteps))
			pixelArea = image[pixelTop+1:pixelTop+11, pixelLeft+1:pixelLeft+14]
			avgColorRGB = np.mean(pixelArea, axis=(0,1))[::-1]
			data[y][x] = get_value_from_cm(avgColorRGB/255.)

	
	print(path)
	dataArray[i%countPerFile] = data
	
	isLast = i == len(allFiles)-1
	if(i%countPerFile == countPerFile-1 or isLast):
		saveFilename = f'{dataFilePath}/data_{channel}_{int(i/countPerFile)}.h5'
		print(f"Saving to '{saveFilename}'")
		h5f = h5py.File(saveFilename, 'w')
		if(isLast):
			dataArray = dataArray[0: i%countPerFile + 1]
		h5f.create_dataset(channel, data=dataArray)
		dataArray = zeroArray
	
	cv2.imshow("", data)
	if(cv2.waitKey(1) == ord("q")):
		break;
# convolutional autoencoder in keras

import os
import sys
import random
random.seed(42)
os.environ["KERAS_BACKEND"] = "tensorflow"

import numpy as np
from keras.layers import Input, Dense, Conv2D, MaxPooling2D, Conv2DTranspose
from keras.models import Sequential
from keras.models import Model
from keras import backend as K
import h5py
f = h5py.File("processed/data_Alpha_0.h5", 'r')
data_mixed =f['Alpha']
data_mixed = np.reshape(data_mixed, data_mixed.shape + (1,))
random.shuffle(data_mixed)
x_train = data_mixed[:2500]
x_test = data_mixed[2500:]

model = Sequential()

model.add(Conv2D(16, (3, 3), activation='relu', padding='same', input_shape=(64, 64, 1)))
model.add(MaxPooling2D((2, 2), padding='same'))
model.add(Conv2D(8, (3, 3), activation='relu', padding='same'))
model.add(MaxPooling2D((2, 2), padding='same'))
model.add(Conv2D(8, (3, 3), activation='relu', padding='same'))
model.add(MaxPooling2D((2, 2), padding='same'))
model.add(Conv2D(8, (3, 3), activation='relu', padding='same'))
model.add(MaxPooling2D((2, 2), padding='same'))
model.add(Conv2D(8, (3, 3), activation='relu', padding='same'))
model.add(MaxPooling2D((2, 2), padding='same'))
model.add(Conv2D(8, (3, 3), activation='tanh', padding='same'))


model.add(Conv2DTranspose(8, (3, 3), strides=(2, 2), activation='relu', padding='same'))
model.add(Conv2DTranspose(8, (3, 3), strides=(2, 2), activation='relu', padding='same'))
model.add(Conv2DTranspose(8, (3, 3), strides=(2, 2), activation='relu', padding='same'))
model.add(Conv2DTranspose(8, (3, 3), strides=(2, 2), activation='relu', padding='same'))
model.add(Conv2DTranspose(1, (3, 3), strides=(2, 2), activation='relu', padding='same'))

model.summary()

model.compile(optimizer='adam', loss='mean_squared_error')
model.fit(x_train, x_train,
                epochs=50,
                batch_size=256,
                shuffle=True,
                validation_data=(x_test, x_test))
import tensorflow as tf
import numpy as np

np.random.seed(42)

import re
import h5py
import datetime
import cv2
import argparse


class AutoDirectionEncoder:
    learning_rate = 0.001
    batch_size = 512
    n_epochs = 1000000
    train_test_split = 0.5
    sliceDepth = 3
    filters = 16
    kernel = (3, 3)
    reuseWeights = False
    steps = 3
    logs_path = "./logs"
    save_path = "./checkpoints"
    restore = False

    def __init__(self, args):
        self.band = args.band
        # Load data
        self.data = self.load_data()
        # create random indices
        random_indices = np.arange(len(self.data) - self.sliceDepth + 1)
        np.random.shuffle(random_indices)
        self.random_train_indices = random_indices[:int(len(self.data) * self.train_test_split)]
        self.random_test_indices = random_indices[int(len(self.data) * self.train_test_split):]
        # build net regular
        self.regularNet = self.build_autoencoder('regular')
        # build net goofy
        self.goofyNet = self.build_autoencoder('goofy')

        self.restore = args.restore

    def load_data(self):
        f = h5py.File(f"processed/data_{self.band}_0.h5", 'r')
        return f[self.band]

    def build_autoencoder(self, name):
        input = tf.placeholder(tf.float32, (None, 64, 64, self.sliceDepth), name='inputs_' + name)
        input_sub = tf.image.resize_bilinear(tf.image.resize_bilinear(input, size=(16, 16)), size=(64, 64))
        x = input_sub

        with tf.variable_scope('ae', reuse=self.reuseWeights):
            for i in range(self.steps):
                x = tf.layers.conv2d(x, self.filters, self.kernel, activation=tf.nn.relu, padding='same')
                x = tf.layers.max_pooling2d(x, pool_size=(2, 2), strides=(2, 2))

            compressed_shape = x.shape
            x = tf.reshape(x, shape=[-1, compressed_shape[1] * compressed_shape[2] * compressed_shape[3]])
            x = tf.layers.dense(x, 128, activation=tf.nn.tanh)
            x = tf.layers.dense(x, 64, activation=tf.nn.tanh)
            # x = tf.layers.dense(x, 32, activation=tf.nn.tanh)
            # # x = tf.layers.dense(x, 4, activation=tf.nn.tanh)
            x = tf.layers.batch_normalization(x)
            #
            hidden_dir = x[:, 0]
            hidden_what = x[:, 1:]
            #
            # x = tf.layers.dense(x, 64, activation=tf.nn.tanh)
            x = tf.layers.dense(x, compressed_shape[1] * compressed_shape[2] * compressed_shape[3],
                                activation=tf.nn.tanh)
            x = tf.reshape(x, shape=[-1, compressed_shape[1], compressed_shape[2], compressed_shape[3]])

            for i in range(self.steps):
                x = tf.layers.conv2d_transpose(x, self.filters, self.kernel, padding='same', activation=tf.nn.relu)
                x = self.upsample(x)

            output = tf.layers.conv2d_transpose(x, self.sliceDepth, self.kernel, padding='same',
                                                activation=tf.nn.sigmoid)

        self.reuseWeights = True

        return input, hidden_dir, hidden_what, output, input_sub

    def upsample(self, x, factor=[2, 2]):
        size = [int(x.shape[1] * factor[0]), int(x.shape[2] * factor[1])]
        y = tf.image.resize_bilinear(x, size=size, align_corners=None, name=None)
        return y

    def mse_subsampled(self, input, output):
        return tf.losses.mean_squared_error(
            tf.image.resize_bilinear(tf.image.resize_bilinear(input, size=(16, 16)), size=(64, 64)), output)

    def get_slice(self, index):
        return np.moveaxis(self.data[index: index + self.sliceDepth], [0, 1, 2], [2, 0, 1])

    def get_batch(self, index, batch_size, indices):
        return [self.get_slice(indices[index] + b) for b in range(batch_size)]

    def goofify_batch(self, batch):
        return np.array(batch)[..., ::-1]

    def train(self):
        # Losses
        regular_input, regular_dir, regular_what, regular_output, regular_input_sub = self.regularNet
        goofy_input, goofy_dir, goofy_what, goofy_output, _ = self.goofyNet

        regular_e2e_loss = tf.losses.mean_squared_error(regular_input, regular_output)
        goofy_e2e_loss = tf.losses.mean_squared_error(goofy_input, goofy_output)
        # directional_loss = tf.losses.mean_squared_error(tf.negative(regular_dir), goofy_dir)
        directional_loss = tf.math.reduce_mean(tf.abs(tf.math.add(regular_dir, goofy_dir)))
        what_loss = tf.losses.mean_squared_error(regular_what, goofy_what)
        total_loss = regular_e2e_loss + goofy_e2e_loss + directional_loss + what_loss

        with tf.variable_scope("train"):

            merged_summary_op_train = tf.summary.merge([tf.summary.scalar("regular_e2e_loss", regular_e2e_loss),
                                                        tf.summary.scalar("goofy_e2e_loss", goofy_e2e_loss),
                                                        tf.summary.scalar("directional_loss", directional_loss),
                                                        tf.summary.scalar("what_loss", what_loss),

                                                        tf.summary.scalar("directional_output_abs",
                                                                          tf.abs(self.regularNet[1][0])),

                                                        tf.summary.scalar("total_loss", total_loss)])

        with tf.variable_scope("test"):
            merged_summary_op_test = tf.summary.merge([tf.summary.scalar("total_loss", total_loss)])

        optimizer = tf.train.AdamOptimizer(learning_rate=self.learning_rate).minimize(total_loss)

        saver = tf.train.Saver()
        # if self.restore:
        #    tf.reset_default_graph()
        #    imported_meta = tf.train.import_meta_graph(f"{self.save_path}/epoch_22500.ckpt.meta")

        with tf.Session() as sess:
            epoch_start = 0

            if self.restore:
                print("Load Model");
                try:
                    with open(f"{self.save_path}/checkpoint") as f:
                        epoch_start = int(re.findall(r'\d+', f.readlines()[0])[0])
                        # Restore variables from disk.
                        saver.restore(sess, tf.train.latest_checkpoint(self.save_path))
                except:
                    pass
            else:
                print("Init Vars");
                sess.run(tf.global_variables_initializer())
            # create log writer object
            writer = tf.summary.FileWriter(datetime.datetime.now().strftime(self.logs_path + "/%A, %d. %B %Y %H-%M/"),
                                           graph=sess.graph)

            for epoch in range(epoch_start, self.n_epochs):
                n_train_batches = int(len(self.random_train_indices) / self.batch_size)
                # Loop over all batches
                for i in range(n_train_batches):
                    batch = self.get_batch(i, self.batch_size, self.random_train_indices)
                    # Run optimization op (backprop) and cost op (to get loss value)
                    _, cost, summary = sess.run([optimizer, total_loss, merged_summary_op_train],
                                                feed_dict={
                                                    regular_input: batch,
                                                    goofy_input: self.goofify_batch(batch)
                                                })
                    # write log
                    writer.add_summary(summary, epoch * n_train_batches + i)

                # test net after each epoch with random test sample
                n_test_batches = int(len(self.random_test_indices) / self.batch_size)
                test_batch = self.get_batch(np.random.randint(0, n_test_batches), 1, self.random_test_indices)
                test_input, test_output, summary_test = sess.run(
                    [regular_input_sub, regular_output, merged_summary_op_test],
                    feed_dict={
                        regular_input: test_batch,
                        goofy_input: self.goofify_batch(test_batch)
                    })
                # write log
                writer.add_summary(summary_test, (epoch + 1) * n_train_batches)

                test_img = np.hstack((self.goofify_batch(test_batch)[0], test_batch[0], test_output[0]))
                cv2.imshow("In/Out", test_img)
                cv2.imwrite("./out.jpg", test_img)
                cv2.waitKey(1)

                # Display logs per epoch step
                print('Epoch', epoch + 1, ' / ', self.n_epochs, 'cost:', cost)

                # save model
                if epoch % 100 == 0:
                    saver.save(sess, f"{self.save_path}/epoch_{epoch}.ckpt")
            print('Optimization Finished')

    def evaluate(self):
        print("Start evaluation")
        regular_input, regular_dir, regular_what, regular_output, regular_input_sub = self.regularNet
        goofy_input, goofy_dir, goofy_what, goofy_output, _ = self.goofyNet

        saver = tf.train.Saver()
        # tf.reset_default_graph()

        with tf.Session() as sess:
            # imported_meta = tf.train.import_meta_graph(f"{self.save_path}/epoch_22500.ckpt.meta")
            # imported_meta.restore(sess, 'my-save-dir/my-model-10000')

            print("Load Model");

            merged_summary_op_train = tf.summary.merge([tf.summary.scalar("directional_output", regular_dir[0] - goofy_dir[0])])
            try:
                with open(f"{self.save_path}/checkpoint") as f:
                    epoch_start = int(re.findall(r'\d+', f.readlines()[0])[0])
                    # Restore variables from disk.
                    saver.restore(sess, tf.train.latest_checkpoint(self.save_path))
            except:
                pass
            # create log writer object
            writer = tf.summary.FileWriter(
                datetime.datetime.now().strftime(self.logs_path + f"/Evaluation {self.band} %A, %d. %B %Y %H-%M/"),
                graph=sess.graph)

            indices = np.arange(len(self.data) - self.sliceDepth + 1)
            n_batches = len(indices)
            # Loop over all batches
            for i in range(n_batches):
                batch = self.get_batch(i, 1, indices)
                # Run optimization op (backprop) and cost op (to get loss value)
                [summary] = sess.run([merged_summary_op_train], feed_dict={regular_input: batch,
                                                                           goofy_input: self.goofify_batch(batch)})
                # write log
                writer.add_summary(summary, i)

            print('Evaluation finished!')


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Train the AutoDirectionEncoder')
    parser.add_argument('--restore', type=bool, default=False, help='Restore checkpoint?')
    parser.add_argument('--evaluate', type=bool, default=False, help='Run evaluation mode on the dataset?')
    parser.add_argument('--band', type=str, default="Beta", help='Which band to use? [Alpha, Beta, Gamma, Delta, Theta]')
    args = parser.parse_args()

    ade = AutoDirectionEncoder(args)
    if (args.evaluate):
        ade.evaluate()
    else:
        ade.train()

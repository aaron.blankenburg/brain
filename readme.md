# Getting started
Install the required pip packages:
```
pip install -r requirements.txt
```

# Run conversion script
```
python data2hd5.py {Channelname}
```